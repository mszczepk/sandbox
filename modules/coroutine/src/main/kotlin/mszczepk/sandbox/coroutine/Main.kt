@file:JvmName("Main")

package mszczepk.sandbox.coroutine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import java.lang.Exception

public fun main(): Unit = runBlocking {
   try {
      runTimeout()
   } catch (e: Exception) {
      println(e)
   }
   println("Elo")
   runAsync(this)
   print("Elo")
}

private suspend fun runTimeout() {
   withTimeout(1300L) {
      repeat(1000) { i ->
         println("I'm sleeping $i ...")
         delay(500L)
      }
   }
}

private suspend fun runBackground(scope: CoroutineScope) {
   scope.launch {
      delay(500L)
      println("Background task finished")
   }
}

private suspend fun runAsync(scope: CoroutineScope): Deferred<String> {
   return scope.async {
      delay(500L)
      println("Async computation completed")
      "Elo"
   }
}
