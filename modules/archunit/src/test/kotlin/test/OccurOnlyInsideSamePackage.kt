package test

import com.tngtech.archunit.core.domain.JavaAccess
import com.tngtech.archunit.lang.ArchCondition
import com.tngtech.archunit.lang.ConditionEvents
import com.tngtech.archunit.lang.SimpleConditionEvent

object OccurOnlyInsideSamePackage : ArchCondition<JavaAccess<*>>("occur only inside the same package") {
   override fun check(item: JavaAccess<*>, events: ConditionEvents) {
      val callerPackage = item.originOwner.`package`
      val itemPackage = item.targetOwner.`package`
      if (callerPackage != itemPackage) {
         val event = SimpleConditionEvent.violated(item, item.description)
         events.add(event)
      }
   }
}
