package packageprivate

import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.all
import test.AccessesToKotlinInternal
import test.OccurOnlyInsideSamePackage
import kotlin.test.Test

class PackagePrivateTest {

   @Test
   fun `all accesses to kotlin internal should occur only inside same package`() {
      // given
      val classes = ClassFileImporter().importPackages("packageprivate")

      // when
      val rule = all(AccessesToKotlinInternal).should(OccurOnlyInsideSamePackage)

      // then
      rule.check(classes)
   }
}
