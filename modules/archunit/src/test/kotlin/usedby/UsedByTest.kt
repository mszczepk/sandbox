package usedby

import com.tngtech.archunit.base.DescribedPredicate.describe
import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import org.junit.jupiter.api.Test

class UsedByTest {

   @Test
   fun `all methods of class A should be annotated with @UsedBy`() {
      // given
      val classes = ClassFileImporter().importPackages("usedby")

      // when
      val rule = methods().that().areDeclaredIn(A::class.java).should().beAnnotatedWith(UsedBy::class.java)

      // then
      rule.check(classes)
   }

   @Test
   fun `methods annotated with @UsedBy should be called only by classes specified in 'clients' parameter`() {
      // given
      val analyzedClasses = ClassFileImporter().importPackages("usedby")

      // when
      val rule = noClasses().should().callMethodWhere(
         describe("method is annotated with @UsedBy and class is not specified in client annotation parameter") { methodCall ->
            methodCall.target.isAnnotatedWith(
               describe("") { annotation ->
                  annotation.rawType.isEquivalentTo(UsedBy::class.java) && methodCall.originOwner !in annotation.properties["clients"] as Array<*>
               }
            )
         }
      )

      // then
      rule.check(analyzedClasses)
   }
}
