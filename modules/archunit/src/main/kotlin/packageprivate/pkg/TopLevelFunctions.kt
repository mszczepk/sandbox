package packageprivate.pkg

fun publicTopLevelGetApiInternalImpl(): Api = InternalImpl()

internal fun internalTopLevelFunction() {}

fun publicTopLevelFunction() {}
