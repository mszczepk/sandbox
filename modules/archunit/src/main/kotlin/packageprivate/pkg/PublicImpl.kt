package packageprivate.pkg

class PublicImpl : Api {
   override fun publicApiMethod() {
      println("public")
   }

   val publicProperty: String = ""

   internal val internalProperty: String = ""

   var publicGetterInternalSetter: String = ""
      internal set

   internal var internalGetterInternalSetter: String = ""

   var publicGetterPublicSetter: String = ""

   @JvmField
   val publicFinalField: String = ""

   @JvmField
   internal val internalFinalField: String = ""

   @JvmField
   var publicMutableField: String = ""

   @JvmField
   internal var internalMutableField: String = ""

   internal fun internalFun() {
      println("public internal func")
   }

   internal class InternalStaticNestedClass {
      fun publicFun() {}
      fun internalFun() {}
   }

   class PublicStaticNestedClass {
      fun internalFun() {}
   }

   inner class PublicNestedClass {
      internal fun internalFun() {}
   }

   internal inner class InternalNestedClass {

   }
}
