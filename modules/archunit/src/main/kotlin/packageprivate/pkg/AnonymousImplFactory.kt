package packageprivate.pkg

object AnonymousImplFactory {
   fun publicGetApiAnonymousImpl(): Api = object : Api {
      override fun publicApiMethod() {
         println("anon")
      }
   }
}
