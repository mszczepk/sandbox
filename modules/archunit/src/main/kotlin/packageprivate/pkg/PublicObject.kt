package packageprivate.pkg

object PublicObject {
   fun publicGetApiInternalImpl(): Api = InternalImpl()

   @JvmStatic
   internal fun internalStatic() {}
}
