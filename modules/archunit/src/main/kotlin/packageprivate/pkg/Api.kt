package packageprivate.pkg

interface Api {
   fun publicApiMethod()

   fun publicInterfaceMethodWithDefaultImpl() {
      println("default impl")
   }
}
