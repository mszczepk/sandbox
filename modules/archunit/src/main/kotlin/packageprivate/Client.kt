package packageprivate

import packageprivate.pkg.AnonymousImplFactory
import packageprivate.pkg.Api
import packageprivate.pkg.InternalImpl
import packageprivate.pkg.PublicObject
import packageprivate.pkg.PublicImpl
import packageprivate.pkg.internalTopLevelFunction
import packageprivate.pkg.publicTopLevelFunction
import packageprivate.pkg.publicTopLevelGetApiInternalImpl

fun main() {

   // PASSES

   // polymorphic access
   PublicObject.publicGetApiInternalImpl().publicApiMethod()

   // calling default impl
   object : Api {
      override fun publicApiMethod() {
         println("custom")
      }
   }.publicInterfaceMethodWithDefaultImpl()

   // file facade usage
   publicTopLevelGetApiInternalImpl().publicApiMethod()

   // anonymous class
   AnonymousImplFactory.publicGetApiAnonymousImpl().publicApiMethod()

   // direct but public access
   PublicImpl().publicInterfaceMethodWithDefaultImpl()

   publicTopLevelFunction()

   PublicImpl().publicGetterInternalSetter

   PublicImpl().publicGetterPublicSetter

   PublicImpl().publicGetterPublicSetter = "new"

   PublicImpl().publicProperty

   PublicImpl().publicFinalField

   PublicImpl().publicMutableField = "new"


   // FAILS
//
//   PublicImpl().publicGetterInternalSetter = "new"
//
//   PublicImpl().internalGetterInternalSetter
//
//   PublicImpl().internalGetterInternalSetter = ""
//
//   PublicImpl().internalProperty
//
//   PublicImpl().internalFun()
//
//   internalTopLevelFunction()
//
//   InternalImpl()
//
//   PublicImpl().internalFinalField
//
//   PublicImpl().internalMutableField = "new"
//
//   PublicImpl.InternalStaticNestedClass()
//
//   PublicImpl.InternalStaticNestedClass().internalFun()
//
//   PublicImpl.PublicStaticNestedClass().internalFun()
//
//   PublicImpl().InternalNestedClass()
//
//   PublicImpl().PublicNestedClass().internalFun()
}
