package usedby

class C {
   fun doSomething(a: A) {
      a.usedByC()
      a.usedByBC()
//      a.usedByB() // test should fail with this line uncommented
   }
}
