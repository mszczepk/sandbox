package usedby

import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.FUNCTION
import kotlin.reflect.KClass

@Retention(RUNTIME)
@Target(FUNCTION)
annotation class UsedBy(vararg val clients: KClass<out Any>)
