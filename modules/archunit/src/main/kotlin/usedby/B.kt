package usedby

class B {
   fun doSomething(a: A) {
      a.usedByB()
      a.usedByBC()
   }
}
