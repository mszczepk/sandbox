package usedby

class A {

   @UsedBy(B::class)
   fun usedByB() {}

   @UsedBy(B::class, C::class)
   fun usedByBC() {}

   @UsedBy(C::class)
   fun usedByC() {}
}
