
public data class Orc (
   val name: String,
   val health: Int,
   val stats: Stats) {

   init {
      if (health > stats.life) {
         throw Exception("health > life ($health > ${stats.life})!")
      }
   }

   public data class Stats(
      val attack: Stat,
      val defense: Stat,
      val life: Int) {

      public data class Stat(
         val min: Int,
         val max: Int) {

         init {
            if (min > max) {
               throw Exception("stat min > max ($min > $max)!")
            }
         }
      }
   }
}
