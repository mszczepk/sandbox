
public class King {

   public fun hire(orc: Orc) {
      if (orc.stats.attack.max < 5) {
         return
      }
      if (orc.health <= 0.1 * orc.stats.life && orc.stats.attack.min < 30) {
         return
      }
      army += orc
   }

   public val armyStrenght: Int
      get() = army.size

   private val army: MutableList<Orc> = mutableListOf()
}
