package test

class TestObjects {

   static deepClone(Map source) {
      def result = [:]
      source.each { k, v ->
         result[k] = v instanceof Map ? deepClone(v) : v
      }
      return result
   }

   static deepMerge(Map base, Map overwrite) {
      def result = deepClone(base)
      overwrite.each { k, v ->
         result[k] = result[k] instanceof Map ? deepMerge(result[k], v) : v
      }
      return result
   }

   static resolve(Map base, Map defaults) {
      def result = deepClone(base)
      base.each { k, v ->
         def node = result[k]
         def defaultNode = defaults[k]
         if (node instanceof Closure) {
            result[k] = node.call(defaultNode)
         } else if (node instanceof Map) {
            result[k] = resolve(node, defaultNode)
         } else {
            result[k] = v
         }
      }
      return result
   }
}

