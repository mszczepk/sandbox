package test

import spock.lang.Specification

class TestObjectsTest extends Specification {

   def 'should deep copy map'() {
      given:
      def map = [a: 1, b: [c: 2, d: [e: 3, f: 4]]]

      when:
      def result = TestObjects.deepClone(map)

      then:
      result == [a: 1, b: [c: 2, d: [e: 3, f: 4]]]
      !result.is(map)
      !result.b.is(map.b)
      !result.b.d.is(map.b.d)
   }

   def 'should deep merge maps'() {
      given:
      def base = [a: 1, animals: [cat: 1, dog: 3]]
      def overwrite = [b: 2, animals: [cat: 2]]

      when:
      def result = TestObjects.deepMerge(base, overwrite)

      then:
      result == [a: 1, b: 2, animals: [cat: 2, dog: 3]]
      !result.is(base)
   }

   def 'should resolve closures into values'() {
      given:
      def base = [a: 1, b: { it + 1 }]
      def defaults = [a: 2, b: 3]

      when:
      def result = TestObjects.resolve(base, defaults)

      then:
      result == [a: 1, b: 4]
   }
}
