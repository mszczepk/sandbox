import spock.lang.Specification

import static TestOrc.orc

class KingTest extends Specification {

   def king = new King()

   def "should hire normal orc"() {
      given:
      def orc = orc()

      when:
      king.hire(orc)

      then:
      king.armyStrenght == 1
   }

   def 'should not hire weak orc'() {
      given:
      def weak = orc(stats: [attack: [min: 0, max: 1]])

      when:
      king.hire(weak)

      then:
      king.armyStrenght == 0
   }

   def 'should not hire wounded orc'() {
      given:
      def wounded = orc(health: { (it * 0.1) as int })

      when:
      king.hire(wounded)

      then:
      king.armyStrenght == 0
   }

   def 'should hire unusually strong orc even if wounded'() {
      given:
      def stronkWounded = orc(
         health: { (it * 0.1) as int },
         stats: [
            attack: [
               min: { it * 3 },
               max: { it * 4 }
            ]
         ]
      )

      when:
      king.hire(stronkWounded)

      then:
      king.armyStrenght == 1
   }
}
