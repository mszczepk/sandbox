import Orc.Stats
import Orc.Stats.Stat

import static test.TestObjects.deepMerge
import static test.TestObjects.resolve

class TestOrc {

   private static final defaults = [
      name: 'Ugh',
      health: 30,
      stats: [
         attack: [
            min: 10,
            max: 15
         ],
         defense: [
            min: 5,
            max: 5
         ],
         life: 30
      ]
   ]

   static Orc orc(Map setup = [:]) {
      def params = deepMerge(defaults, setup)
      params = resolve(params, defaults)

      return new Orc(
         params.name,
         params.health,
         new Stats(
            new Stat(
               params.stats.attack.min,
               params.stats.attack.max
            ),
            new Stat(
               params.stats.defense.min,
               params.stats.defense.max
            ),
            params.stats.life
         )
      )
   }
}
