import io.reactivex.rxjava3.core.Single
import spock.lang.Specification
import spock.lang.Unroll

import static FooBarMergeException.TooManyFoosException
import static FooBarMergeException.TooManyBarsException

@Unroll
class FooBarMergeTest extends Specification {

   def 'should merge foo with bars (#subject.getClass().getSimpleName())'() {
      given:
      def foos = Single.just([
         new Foo('1', 'a'),
         new Foo('2', 'foo')
      ])
      def bars = Single.just([
         new Bar('1', 'A'),
         new Bar('3', 'bar')
      ])
      def expected = [
         new FooBar('1', 'aA'),
         new FooBar('2', 'foo'),
         new FooBar('3', 'bar')
      ] as Set

      expect:
      subject.merge(foos, bars).test().values() as Set == expected

      where:
      subject << [
         VisitorFooBarMerge.INSTANCE,
         MapReduceFooBarMerge.INSTANCE
      ]
   }

   def 'should fail if too many foos provided (#subject.getClass().getSimpleName())'() {
      given:
      def id = '1'
      def foos = Single.just([
         new Foo(id, 'foo1'),
         new Foo(id, 'foo2')
      ])
      def bars = Single.just([
         new Bar(id, 'bar')
      ])

      expect:
      subject.merge(foos, bars).test().assertError {
         it instanceof TooManyFoosException && ((TooManyFoosException) it).id == id
      }

      where:
      subject << [
         VisitorFooBarMerge.INSTANCE,
         MapReduceFooBarMerge.INSTANCE
      ]
   }

   def 'should fail if too many bars provided (#subject.getClass().getSimpleName())'() {
      given:
      def id = '1'
      def foos = Single.just([
         new Foo(id, 'foo')
      ])
      def bars = Single.just([
         new Bar(id, 'bar1'),
         new Bar(id, 'bar2')
      ])

      expect:
      subject.merge(foos, bars).test().assertError {
         it instanceof TooManyBarsException && ((TooManyBarsException) it).id == id
      }

      where:
      subject << [
         VisitorFooBarMerge.INSTANCE,
         MapReduceFooBarMerge.INSTANCE
      ]
   }
}
