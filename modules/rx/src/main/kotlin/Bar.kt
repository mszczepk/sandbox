import FooBar.FooBarBuilder

data class Bar(
   override val id: String,
   val bar: String
): GroupableById {

   override fun accept(visitor: FooBarBuilder): FooBarBuilder = visitor.visit(this)
}
