import FooBarMergeException.TooManyBarsException
import FooBarMergeException.TooManyFoosException

data class FooBar constructor(
   val id: String,
   val foobar: String
) {

   class FooBarBuilder(private val id: String) {
      private var foo: Foo? = null
      private var bar: Bar? = null

      fun visit(f: Foo): FooBarBuilder {
         require(f.id == id)
         if (foo != null) throw TooManyFoosException(id)
         foo = f
         return this
      }

      fun visit(b: Bar): FooBarBuilder {
         require(b.id == id)
         if (bar != null) throw TooManyBarsException(id)
         bar = b
         return this
      }

      fun build(): FooBar {
         return FooBar(id, (foo?.foo ?: "") + (bar?.bar ?: ""))
      }
   }
}
