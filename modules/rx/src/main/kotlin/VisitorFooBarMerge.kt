import FooBar.FooBarBuilder
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.flowables.GroupedFlowable

/*
 * Note:
 * This implementation pollutes FooBar classes with the visitor pattern.
 * It may be acceptable from the dependency INVERSION principle point of view.
 */
object VisitorFooBarMerge : FooBarMerge {

   override fun merge(fooBatch: Single<List<Foo>>, barBatch: Single<List<Bar>>): Flowable<FooBar> {
      val foos = fooBatch.toFlow()
      val bars = barBatch.toFlow()

      return Flowable.merge(foos, bars)
         .groupBy { it.id }
         .flatMapSingle { it.reduceToFooBar() }
         .map { it.build() }
   }

   private fun <T : Any> Single<List<T>>.toFlow(): Flowable<T> =
      flatMapPublisher { Flowable.fromIterable(it) }

   private fun GroupedFlowable<String, GroupableById>.reduceToFooBar(): Single<FooBarBuilder> =
      reduce(FooBarBuilder(key!!)) { builder, item -> builder.append(item) }

   private fun FooBarBuilder.append(groupableById: GroupableById): FooBarBuilder =
      groupableById.accept(this)
}
