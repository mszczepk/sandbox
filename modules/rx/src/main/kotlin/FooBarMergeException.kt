sealed class FooBarMergeException(
   val id: String, override val message: String? = null
) : IllegalArgumentException() {

   class TooManyFoosException(id: String) : FooBarMergeException(id, "Too many foos encountered for id: $id!")

   class TooManyBarsException(id: String) : FooBarMergeException(id, "Too many bars encountered for id: $id!")
}
