import FooBarMergeException.TooManyBarsException
import FooBarMergeException.TooManyFoosException
import MapReduceFooBarMerge.GroupableById.BarById
import MapReduceFooBarMerge.GroupableById.FooById
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.flowables.GroupedFlowable

object MapReduceFooBarMerge : FooBarMerge {

   override fun merge(fooBatch: Single<List<Foo>>, barBatch: Single<List<Bar>>): Flowable<FooBar> {
      val foos = fooBatch
         .toFlow()
         .map { FooById(it) }
      val bars = barBatch
         .toFlow()
         .map { BarById(it) }

      return Flowable.merge(foos, bars)
         .groupBy { it.id }
         .flatMapSingle { toFooBar(it) }
   }

   private fun toFooBar(group: GroupedFlowable<String, GroupableById>): Single<FooBar> {
      return group
         .reduce(FooBarBuilder(group.key!!)) { builder, grouped -> builder.append(grouped) }
         .map { it.build() }
   }

   private fun <T : Any> Single<List<T>>.toFlow(): Flowable<T> =
      flatMapPublisher { Flowable.fromIterable(it) }

   private sealed class GroupableById(val id: String) {

      data class FooById(val foo: Foo) : GroupableById(foo.id)

      data class BarById(val bar: Bar) : GroupableById(bar.id)
   }

   private data class FooBarBuilder(val id: String, val foo: Foo? = null, val bar: Bar? = null) {

      fun append(groupped: GroupableById): FooBarBuilder =
         when (groupped) {
            is FooById -> {
               if (foo != null) throw TooManyFoosException(id)
               copy(foo = groupped.foo)
            }
            is BarById -> {
               if (bar != null) throw TooManyBarsException(id)
               copy(bar = groupped.bar)
            }
         }

      fun build(): FooBar {
         require(foo != null || bar != null)
         return FooBar(id, (foo?.foo ?: "") + (bar?.bar ?: ""))
      }
   }
}
