import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

interface FooBarMerge {

   fun merge(fooBatch: Single<List<Foo>>, barBatch: Single<List<Bar>>): Flowable<FooBar>
}
