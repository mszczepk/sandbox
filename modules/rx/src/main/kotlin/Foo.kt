data class Foo(
   override val id: String,
   val foo: String
) : GroupableById {

   override fun accept(visitor: FooBar.FooBarBuilder): FooBar.FooBarBuilder = visitor.visit(this)
}
