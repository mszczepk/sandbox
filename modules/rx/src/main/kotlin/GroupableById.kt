import FooBar.FooBarBuilder

interface GroupableById {
   val id: String

   fun accept(visitor: FooBarBuilder): FooBarBuilder
}
