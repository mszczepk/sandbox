
public data class Orc (
   val name: String,
   val stats: Stats = Stats(),
   val health: Int = stats.life,
) {

   init {
      if (health > stats.life) {
         throw Exception("health > life ($health > ${stats.life})!")
      }
      if (health <= 0) {
         throw Exception("Orc must be alive at the beginning of its journey!")
      }
   }

   public data class Stats(
      val attack: Stat = Stat(),
      val defense: Stat = Stat(),
      val life: Int = 30) {

      public data class Stat(
         val min: Int = 10,
         val max: Int = 15) {

         init {
            if (min > max) {
               throw Exception("stat min > max ($min > $max)!")
            }
         }
      }
   }
}
