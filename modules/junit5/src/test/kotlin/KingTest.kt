import Orc.Stats
import Orc.Stats.Stat
import org.amshove.kluent.invoking
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldThrow
import org.amshove.kluent.withMessage
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.lang.Exception

internal class KingTest {

   @Test
   fun `should hire normal orc`() {
      val king = King()
      val orc = Orc(name = "Roman")

      king.hire(orc)

      king.armyStrenght shouldBeEqualTo 1
   }

   @Test
   fun `should not hire weak orc`() {
      val king = King()
      val orc = Orc(
         name = "Macaroni",
         stats = Stats(
            attack = Stat(min = 1, max = 3)
         )
      )

      king.hire(orc)

      king.armyStrenght shouldBeEqualTo 0
   }

   @Test
   fun `should fail to create dead orc`() {
      invoking { Orc(name = "Hugo", health = 0) }
         .shouldThrow(Exception::class)
         .withMessage("Orc must be alive at the beginning of its journey!")
   }

   @TestFactory
   fun `should (not) hire orc depending on its condition`() = listOf(
      Tuple5(0        , 1        , 30    , 0           , "weak"              ),
      Tuple5(10       , 15       , 30    , 1           , "normal"            ),
      Tuple5(10       , 15       , 2     , 0           , "normal but wounded"),
      Tuple5(100      , 150      , 30    , 1           , "strong"            ),
      Tuple5(100      , 150      , 2     , 1           , "strong but wounded"),
   ).map {  (minAttack, maxAttack, health, armyStrength, description         ) ->

      dynamicTest("should ${if (armyStrength == 0) "not " else " "}hire orc ($description)") {
         // given
         val king = King()
         val orc = Orc(
            name = "Max",
            stats = Stats(
               attack = Stat(minAttack, maxAttack),
            ),
            health = health,
         )

         // when
         king.hire(orc)

         // then
         king.armyStrenght shouldBeEqualTo armyStrength
      }
   }

   @TestFactory
   fun `should (not) hire orc depending on its condition (pretty version)`() = test(
      dataset(0        , 1        , 30    , 0           , "weak"              ),
      dataset(10       , 15       , 30    , 1           , "normal"            ),
      dataset(10       , 15       , 2     , 0           , "normal but wounded"),
      dataset(100      , 150      , 30    , 1           , "strong"            ),
      dataset(100      , 150      , 2     , 1           , "strong but wounded"),) forEach {
             (minAttack, maxAttack, health, armyStrength, description         ) ->

      // given
      val king = King()
      val orc = Orc(
         name = "Ur-Shak",
         stats = Stats(
            attack = Stat(minAttack, maxAttack),
         ),
         health = health,
      )

      // when
      king.hire(orc)

      // then
      king.armyStrenght shouldBeEqualTo armyStrength
   }

   private class test<T>(vararg datasets: T) {

      private val datasets: List<T> = datasets.toList()

      infix fun forEach(testBody: (T) -> Unit): List<DynamicTest> =
         datasets.mapIndexed { index, dataset ->
            dynamicTest("test $index") { testBody.invoke(dataset) }
         }
   }

   private fun <P1, P2, P3, P4, P5> dataset(p1: P1, p2: P2, p3: P3, p4: P4, p5: P5) = Tuple5(p1, p2, p3, p4, p5)

   private data class Tuple5<P1, P2, P3, P4, P5>(
      val p1: P1,
      val p2: P2,
      val p3: P3,
      val p4: P4,
      val p5: P5,
   )
}
