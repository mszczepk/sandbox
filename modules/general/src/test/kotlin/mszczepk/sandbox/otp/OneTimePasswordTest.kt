package mszczepk.sandbox.otp

import java.time.Instant
import java.util.UUID
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import kotlin.math.pow
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.text.Charsets.UTF_8
import kotlin.Long.Companion.SIZE_BYTES as LONG_SIZE_BYTES

class OneTimePasswordTest {

   @Test
   fun `should generate one time password based on time and shared secret`() {
      // given
      val secretKey = UUID.fromString("f8225e7d-3092-490f-8b62-8838ba922ddf").toHmacSHA1CompilantSecretKey()
      val timestamp = Instant.parse("2025-01-20T12:00:00Z")

      val otp = generateOneTimePassword(secretKey, timestamp)

      assertEquals(actual = otp, expected = "958145")
   }

   @Test
   fun `should return byte as hex number string`() {
      // given
      val bytes = byteArrayOf(0x4f, 0x56, 0x72, 0x9a.toByte(), 0x33, 0x55, 0x12, 0xa2.toByte())

      // expect
      assertEquals(actual = bytes.toHexNumberString(), expected = "4f56729a335512a2")
   }

   fun generateHmac(secretKey: String, data: String): String {
      val secretKeyBytes = secretKey.toByteArray(UTF_8)
      val dataBytes = data.toByteArray(UTF_8)
      val mac = Mac.getInstance("HmacSHA1")
      val secretKeySpec = SecretKeySpec(secretKeyBytes, "HmacSHA1")
      mac.init(secretKeySpec)

      return mac.doFinal(dataBytes).toHexNumberString()
   }

   fun ByteArray.toHexNumberString() =
      joinToString(separator = "") { "%02x".format(it) }

   fun generateOneTimePassword(
      secretKey: String,
      timestamp: Instant,
      timeWindowSeconds: Int = 30,
      length: Int = 6,
   ): String {
      val data = (timestamp.epochSecond / timeWindowSeconds).toString()
      val hmac = generateHmac(secretKey, data)
      println("hmac: $hmac")
      val otp = hmac.hmacToDigits(length = length)
      println("otp: $otp")
      return otp
   }

   fun UUID.toHmacSHA1CompilantSecretKey() =
      toString()
         .replace("-", "") // HMAC SHA-1 does not accept anything else than alphanumerical chars
         .substring(0, 16)

   /**
    * @receiver Must be a HMAC result representing a number in the hexadecimal system.
    * @param length How many digits we want to get from the HMAC result.
    */
   fun String.hmacToDigits(length: Int = 6): String {
      val hmac = this
      require(hmac.length == 40) // HMAC SHA-1 always has 20 bytes, which is a number with 40 digits in hex.
      require(length > 0)
      val lastDigitAsHexString = hmac.last() // It's a hex digit [0; f]
      val dynamicOffset = lastDigitAsHexString.hexToInt() * 2 // Offset is [0;30] = [0;15] * 2. It's just a way to select an offset based on the HMAC value (note it's always lower than 40, which is the length of HMAC).
      val longNumberAsHexString = hmac.substring(dynamicOffset, dynamicOffset + LONG_SIZE_BYTES)
      val longNumber = longNumberAsHexString.hexToLong()
      val longNumberNonNegative = longNumber.toNonNegativeByMaskingMostSignificantBit()
      val howManyDigitsDivisor = 10.0.pow(length).toLong()
      val number = longNumberNonNegative % howManyDigitsDivisor
      return "%06d".format(number) // pad zeros
   }

   fun Char.hexToInt() = digitToInt(radix = 16)

   fun String.hexToLong() = toLong(radix = 16)

   /**
    * 32 bit (8 byte) number (fits long variable):
    *
    * `11111111111111111111111111111111` (bin) =         -1 (dec)
    *
    * `01111111111111111111111111111111` (bin) = 2147483647 (dec)
    */
   fun Long.toNonNegativeByMaskingMostSignificantBit() =
      this and 0x7fffffff
}
