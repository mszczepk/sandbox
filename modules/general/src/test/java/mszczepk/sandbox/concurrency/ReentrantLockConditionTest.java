package mszczepk.sandbox.concurrency;

import mszczepk.sandbox.Utils.UncheckedThread;
import org.junit.Test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class ReentrantLockConditionTest {

   @Test
   public void lockConditionShouldWorkSameAsSyncBlockVersion() throws Exception {
      var syncObj = new WithCondition();
      var t3 = new UncheckedThread(() -> syncObj.doJob(3));
      t3.start();
      var t2 = new UncheckedThread(() -> syncObj.doJob(2));
      t2.start();
      var t1 = new UncheckedThread(() -> syncObj.doJob(1));
      t1.start();

      t3.join();
      t2.join();
      t1.join();
   }

   static class WithCondition {
      private boolean isBusy = false;
      private final Lock lock = new ReentrantLock();
      private final Condition condition = lock.newCondition();

      void doJob(int thread) throws Exception {
         lock.lock();
         while (isBusy) {
            System.out.printf("Waiting %d%n", thread);
            condition.await();
            System.out.printf("Resuming %d%n", thread);
         }
         System.out.printf("Started %d%n", thread);
         isBusy = true;
         lock.unlock();
         Thread.sleep(thread * 2000L);
         System.out.printf("Done %d%n", thread);
         lock.lock();
         condition.signalAll();
         isBusy = false;
         lock.unlock();
      }
   }
}
