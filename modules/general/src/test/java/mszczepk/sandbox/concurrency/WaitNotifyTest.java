package mszczepk.sandbox.concurrency;

import mszczepk.sandbox.Utils.UncheckedThread;
import org.junit.Test;

import static java.lang.Thread.sleep;

public class WaitNotifyTest {

   @Test
   public void waitNotifyWithSynchronizedBlock() throws Exception {
      var syncObj = new WithSyncBlock();
      var t3 = new DoJobThread(syncObj, 3);
      t3.start();
      var t2 = new DoJobThread(syncObj, 2);
      t2.start();
      var t1 = new DoJobThread(syncObj, 1);
      t1.start();

      t3.join();
      t2.join();
      t1.join();
   }

   static class WithSyncBlock implements SyncObj {
      boolean isBusy = false;

      // wait/notify needs to be inside in synchronized block or method to work
      @Override
      public void doJob(int thread) throws Exception {
         synchronized (this) {
            while (isBusy) {
               System.out.printf("Waiting %d%n", thread);
               wait();
               System.out.printf("Resuming %d%n", thread);
            }
            System.out.printf("Started %d%n", thread);
            isBusy = true;
         }
         sleep(thread * 2000L);
         System.out.printf("Done %d%n", thread);
         synchronized (this) {
            isBusy = false;
            notifyAll();
         }
      }
   }

   // doesn't make much sense in this use case because effectively sequential
   @Test
   public void waitNotifyWithSynchronizedMethod() throws Exception {
      var syncObj = new WithSyncMethod();
      var t3 = new DoJobThread(syncObj, 3);
      t3.start();
      var t2 = new DoJobThread(syncObj, 2);
      t2.start();
      var t1 = new DoJobThread(syncObj, 1);
      t1.start();

      t3.join();
      t2.join();
      t1.join();
   }

   static class WithSyncMethod implements SyncObj {
      boolean isBusy = false;

      @Override
      public synchronized void doJob(int thread) throws Exception {
         while (isBusy) {
            System.out.printf("Waiting %d%n", thread);
            wait();
            System.out.printf("Resuming %d%n", thread);
         }
         System.out.printf("Started %d%n", thread);
         isBusy = true;
         sleep(thread * 2000L);
         System.out.printf("Done %d%n", thread);
         isBusy = false;
         notifyAll();
      }
   }

   @Test
   public void waitNotifyWithoutObjectMonitor() throws Exception {
      var syncObj = new WithoutObjectMonitor();
      var t3 = new DoJobThread(syncObj, 3);
      t3.start();
      var t2 = new DoJobThread(syncObj, 2);
      t2.start();
      var t1 = new DoJobThread(syncObj, 1);
      t1.start();

      t3.join();
      t2.join();
      t1.join();
   }

   // wait/notify methods throw IllegalMonitorStateException
   static class WithoutObjectMonitor implements SyncObj {
      boolean isBusy = false;

      @Override
      public void doJob(int thread) throws Exception {
         while (isBusy) {
            System.out.printf("Waiting %d%n", thread);
            wait();
            System.out.printf("Resuming %d%n", thread);
         }
         System.out.printf("Started %d%n", thread);
         isBusy = true;
         sleep(thread * 2000L);
         System.out.printf("Done %d%n", thread);
         isBusy = false;
         notifyAll();
      }
   }

   interface SyncObj {
      void doJob(int thread) throws Exception;
   }

   static class DoJobThread extends UncheckedThread {
      DoJobThread(SyncObj syncObj, int i) {
         super(() -> syncObj.doJob(i));
      }
   }
}
