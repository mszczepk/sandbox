package mszczepk.sandbox;

import static java.util.Objects.requireNonNull;

public final class Utils {

   public static class UncheckedThread extends Thread {
      private final ThrowingRunnable runnable;

      public UncheckedThread(ThrowingRunnable runnable) {
         requireNonNull(runnable);
         this.runnable = runnable;
      }

      @Override
      public void run() {
         try {
            runnable.run();
         } catch (Exception e) {
            throw new RuntimeException(e);
         }
      }
   }

   @FunctionalInterface
   public interface ThrowingRunnable {
      void run() throws Exception;
   }
}
