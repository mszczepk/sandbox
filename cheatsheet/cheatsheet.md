# Concurrency

## Synchronized method (keyword)
Tylko jeden wątek może wykonywać daną metodę na danym obiekcie (lub klasie, jeśli dotyczy funkcji
statycznej). W kotlinie uzyskuje się przez adnotację `@Synchronized`.
```
class Elo {
   synchronized void doJob() {}
}
...
var elo = Elo();
startThreads(threads = 3) {
   elo.doJob();
}
```

## Synchronized block
Tylko jeden wątek może mieć dostęp do zasobu w danym momencie.
```
class Elo {
   void doJob() {
      synchronized(this) { // here we use “this” as a locked object, but it can be anything (like “Elo.class”)
      ...
      }
   }
}
...
var elo = Elo();
startThreads(threads= 3) {
   elo.doJob();
}
```

## Wait / Notify
Wait i notify muszą być umieszczane wewnątrz synchronizowanych bloków albo funkcji, bo inaczej może
polecieć exception. Synchronizowane bloki albo funkcje zakładają tzn. monitor na obiekcie którego
dotyczą, a wait/notify korzystają z mechanizmu monitorów.
```
import static java.lang.Thread.sleep;

class WaitNotify {
   public static void main(String[] args) throws Exception {
      test(new WithSyncBlock());
      test(new WithSyncMethod()); // sequential access
      test(new WithoutObjectMonitor()); // fails
   }

   private static void test(SyncObj syncObj) throws Exception {
      System.out.printf("Testing %s%n", syncObj.getClass().getSimpleName());
      var t3 = new DoJobOnSyncObjThread(syncObj, 3);
      t3.start();
      var t2 = new DoJobOnSyncObjThread(syncObj, 2);
      t2.start();
      var t1 = new DoJobOnSyncObjThread(syncObj, 1);
      t1.start();

      t3.join();
      t2.join();
      t1.join();
      System.out.printf("All done%n");
   }
}

class DoJobOnSyncObjThread extends Thread {
   private final SyncObj syncObj;
   private final int i;

   DoJobOnSyncObjThread(SyncObj syncObj, int i) {
      this.syncObj = syncObj;
      this.i = i;
   }

   @Override
   public void run() {
      try {
         syncObj.doJob(i);
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }
}

interface SyncObj {
   void doJob(int thread) throws Exception;
}

class WithSyncBlock implements SyncObj {
   boolean isBusy = false;

   @Override
   public void doJob(int thread) throws Exception {
      synchronized (this) { // "wait" needs to be inside in synchronized block or method to work
         while (isBusy) {
            System.out.printf("Waiting %d%n", thread);
            wait();
            System.out.printf("Resuming %d%n", thread);
         }
         System.out.printf("Started %d%n", thread);
         isBusy = true;
      }
      sleep(thread * 2000L);
      System.out.printf("Done %d%n", thread);
      synchronized (this) {  // "notify" needs to be inside in synchronized block or method to work
         isBusy = false;
         notifyAll();
      }
   }
}

class WithSyncMethod implements SyncObj {
   boolean isBusy = false;
   
   @Override
   public synchronized void doJob(int thread) throws Exception {
      while (isBusy) {
         System.out.printf("Waiting %d%n", thread);
         wait();
         System.out.printf("Resuming %d%n", thread);
      }
      System.out.printf("Started %d%n", thread);
      isBusy = true;
      sleep(thread * 2000L);
      System.out.printf("Done %d%n", thread);
      isBusy = false;
      notifyAll();
   }
}

class WithoutObjectMonitor implements SyncObj {
   boolean isBusy = false;

   @Override
   public void doJob(int thread) throws Exception {
      while (isBusy) {
         System.out.printf("Waiting %d%n", thread);
         wait();
         System.out.printf("Resuming %d%n", thread);
      }
      System.out.printf("Started %d%n", thread);
      isBusy = true;
      sleep(thread * 2000L);
      System.out.printf("Done %d%n", thread);
      isBusy = false;
      notifyAll();
   }
}
```
Czyli w skrócie, wait/notify (albo `Condition` z klasy `ReentrantLock`) służą optymalizacji, żeby 
nie robić "busy-waiting", ani "Thread.sleep()", czyli żeby pętla nie sprawdzała warunku tak szybko,
jak to możliwe.

## Mutex (Mutual Exclusion Lock) = ReentrantLock
Tylko jeden wątek może mieć dostęp do zasobu w danym momencie.
```
val lock = ReentrantLock();
startTheads(threads = 3) {
   lock.lock() // blokujemy zasob, inne watki czekają
   doJob()
   lock.unlock() // odblokowujemy zasób, inne wątki mogą wznowić pracę, najlepiej dać w “finally”
}
```
Jest też wersja `ReentrantReadWriteLock`:
* sekcje odczytu i zapisu
* wiele wątków może odczytywać na raz, ale tylko jeden może zapisywać
* zapis blokuje sekcje zarówno odczytu i jak i zapisu

## Semaphore
Trzeba ograniczyć liczbę wątków mających dostęp do zasobu. To samo co Mutex, jeśli dasz 1 "permit".
```
val semaphore = Semaphore(permits = 2);
startTheads(threads = 3) { // more threads than permits
   semaphore.acquire() // two threads can enter the critical section, the 3rd one must wait
   doJob()
   semaphore.release() // the 3rd thread can enter the section now
}
```

## CountDownLatch
Wątek chce poczekać aż inne wątki dotrą do jakiegoś etapu (np. zakończą pracę). Można użyć tylko raz.
```
val latch = CountDownLatch(3) // main thread will wait for 3 (say the counter = 3)
startThreads(threads = 3) {
   doJob()
   latch.countDown() // signals that the job is done
}
latch.await() // blocks until 3 other threads send the signal (waits until the counter == 0)
```

## CyclicBarrier
Wątki chcą poczekać na siebie na wzajem, aż wszystkie dotrą do tego samego etapu. Można robić w pętli.
```
val barrier = CyclicBarrier(3) // there will be 3 threads in a party
startThreads(threads = 3) {
   while (true) {
      doJobPart1()
      barrier.await() // blocks until 2 other threads reaches the same point
      doJobPart2()
   }
}
// main thread does not participate in the party
```

## Phaser
CountDownLatch + CyclicBarrier + liczy liczbę cykli które przechodzi bariera (fazy)
```
val phaser = Phaser(3) // there will be 3 threads in a party
startThreads(threads = 3) {
   while (true) {
      doJobPart1()
      phaser.arriveAndAwaitAdvance() // blocks until 2 other threads reaches the same point
      doJobPart2()
   }
}
phaser.avaitAdvance(phase = 4) // main thread waits until the other threads run 4 cycles
```

## Deadlock vs Livelock vs Starvation
| nazwa          | opis                                                                                                                   |
|----------------|------------------------------------------------------------------------------------------------------------------------|
| deadlock       | wątki wzajemnie się blokują, bo czekają na zasób który trzyma drugi wątek                                              |
| livelock       | wątki nie są zablokowane, ale nie robią też postępu, bo na zmianę dają sobie sygnały do zatrzymywania/wznawiania pracy |
| starvation     | wątek czeka w kolejce do zasobu                                                                                        |
| race condition | wynik programu jest niedeterministyczny (i błędny), bo zależy od (losowej) kolejności w jakiej wątki zmieniały dane    |

`counter++` nie jest atomową operacją dla typu INT, bo wymaga odczytu, dodania i zapisu, w międzyczasie wartość może się zmienić, używaj Atomic Integer, albo synchronizuj

# Struktury danych
https://www.bigocheatsheet.com/

![data-structures-time-complexity.png](img/data-structures-time-complexity.png)

## b-tree vs binary search tree
![btree-bst-diff.png](img/btree-bst-diff.png)
oba drzewa pozwalają na wyszukiwanie zakresów

## Filtr Blooma
Probabilistyczna struktura danych do sprawdzania czy coś znajduje się w zbiorze lub nie.
Cechy:
* Dla dużych zbiorów zajmuje mniej pamięci, niż tradycyjne trzymanie wszystkich elementów
* Można dodawać, nie można usuwać elementy (chociaż jest wersja Counting Bloom Filter - tylko więcej
  pamięci wymaga)
* Sprawdzenie, czy element istnieje jest bardzo szybkie
* Sprawdzenie, czy element istnieje, ma dwa możliwe wyniki: prawdopodobnie istnieje (false positive), 
  albo na pewno nie istnieje (false negative)
  * Oznacza to, że jest jakiś margines błędu, zależy to od konfiguracji: ile chcemy pamięci przeznaczyć
    na filtr i ile funkcji haszujących użyć
  * Jeśli zależy nam na 100% pewności że coś istnieje, musimy zrobić dodatkowy check, ALE udaje się zoptymalizować znakomitą większość
  * Jeśli duplikaty w systemie zdarzają się rzadko, to niepewne sytuacje “może istnieje” (które wymagają więcej zasobów)  będą występować rzadziej niż sytuacje pewne “na pewno nie istnieje”, więc uzyskujemy duży wzrost wydajności

Zastosowanie:
* Optymalizacja keszy (optymalizacja kesze hit)
* Optymalizacja indeksów
* Deduplikacja (np. eventów)

# System Design Interview
Co musisz znać?
* Podstawowe koncepcje i wzorce projektowe
* Load balancing: Round-robin, least connections, etc.
* Caching: Lokalny cache (in-memory), rozproszony cache (Redis, Memcached).
* Database design: Relacyjne (SQL) vs. nierelacyjne (NoSQL), partycjonowanie, replikacja, indeksowanie.
* Sharding: Jak podzielić dane na wiele baz danych.
* Consistency models: Strong, eventual consistency.
* Message queues: RabbitMQ, Kafka, pub/sub pattern.
* Microservices vs. Monoliths: Zalety, wady, kiedy stosować.
* Rate limiting: Algorytmy (np. token bucket, leaky bucket).
* CDN (Content Delivery Network): Jak działają i kiedy je stosować.
* Monitoring i logowanie: Prometheus, Grafana, ELK stack.
* Popularne architektury i ich zastosowanie

Nie musisz znać każdego szczegółu, ale dobrze wiedzieć, jak działają:
* Client-server architecture: Typowe dla aplikacji webowych.
* Microservices: Jak rozdzielać funkcjonalności, komunikacja między serwisami (np. gRPC, REST).
* Event-driven architecture: Kiedy używać eventów zamiast zapytań synchronicznych.
* Master-slave vs. leader-follower: Dla baz danych i systemów rozproszonych.
* Serverless: AWS Lambda, Azure Functions – kiedy opłaca się używać.
* CAP theorem: Trade-off między Consistency, Availability i Partition tolerance

# Content Delivery Network (CDN)
* streamowanie video
* pliki statyczne: zdjęcia, css, js, aplikacje do pobrania?
* zmniejsza opóźnienia w pobieraniu dużych plików bo:
  * jest keszem na dane (origin)
  * trzyma dane w lokalizacji najbliżej użytkownika Point of Presence (POP)
  * automatycznie kieruje użytkownika do odpowiedniego POP (routing)
  * używa keszowania
  * odciąża twoje serwisy backendowe

CDN trzeba skonfigurować, żeby bucket w cloud storage był źródłem danych:
* Backend generuje ID uploadowanego pliku wideo
* Zapisujemy plik w cloud storage, w buckecie (np. GCS)
* Konfigurujemy CDN (np. Google Cloud CDN) żeby korzystał z bucketa jako źródła danych.
  Do konfiguracji używamy np. Google Cloud Console
* Backend zwraca linki do CDN a nie do GCS

CDN to przykład “edge computing”

Co jeśli dane w origin się zmienią?
1) CDN można wysłać request o invalidate cache
   * mogą zostać nałożone opłaty
   * nie rozwiązuje problemu
2) Wersjonowanie  
   Backend generuje linki z wersją i nigdy nie zmienia plików, zawsze tworzy nowe. Zwraca też te linki frontendowi.
   * nie jest ok, jeśli user zna z góry dany adres, nie zawsze to zadziała?
   * trzeba zorganizować proces czyszczący stare wersje
3) Konfiguracja nagłówków  
   `Cache-Control: max-age=60`  
   `E-tag` + `If-None-Match` (jeśli oba nagłówki przychodzą z origin i od klienta pobierającego)

Public API
DNS - Domain Name System
Prowadz
Data Locality + Regiony
Database Partitioning Across Regions

DNS (Domain Name System)
Typy rekordów DNS:
A - jeśli chcesz przypisać domenę do stałego adresu IP,
np. example.com A 123.212.31.21
CNAME - jeśli chcesz zrobić subdomenę,
np. cdn.example.com CNAME example.com
NS - tylko dla serwerów DNS, wskazuje który autorytatywny serwer DNS odpowiada za daną domenę
np. example.com NS ns1.googlednsapis.com

Nazwy DNS (np. example.com) trzeba zarejestrować u dostawcy (np. Google Domains, Cloudflare) + trzeba zapłacić.

Gdy mam już swoją domenę, mogę przypisać ją do adresu IP load balancera korzystając z jakiegoś panelu właściciela domeny. Spowoduje to aktualizację rekordów A i CNAME w autorytatywnym serwerze DNS oraz po jakimś czasie odświeżenie keszy w całym systemie DNS.

Ogólnie serwery DNS są hierarchiczne.

DNS geo routing - systemy przekierowują cię do serwerów które są najbliżej twojej lokalizacji

# Java
## Nowości
| wersja  | co dodano                                                                                                                |
|---------|--------------------------------------------------------------------------------------------------------------------------|
| 8       | Lambda, Stream API, Optional, G1 GC                                                                                      |
| 9       | Modules                                                                                                                  |
| 10 - 14 | Var, Records, Pattern Matching (`obj instanceof String s`)                                                               |
| 15 - 17 | Sealed Classes/Interfaces, Text Blocks, Z GC                                                                             |
| 19 - 21 | Virtual Threads, Vector API (do operacji graficznych), structured concurrency (proste API z klasą `StructuredTaskScope`) |

## Unboxing
```
var i = Integer.valueOf(0); // i has type Integer
i++;
System.out.println(i); // prints 1
```
Integer jest immutable w java, ale mamy też unboxing. W drugiej linijce dzieje się kilka rzeczy:
* Zmienna `i` jest typu `Integer` więc jest unboxowane na typ `int` (bo `Integer` nie wspiera
  operatora `++`)
* Operator `++` jest używany na typie `int`, równoważne z `i = i + 1`
* Z racji tego że wynik jest typu `int`, a zmienna `i` typu `Integer`, to wynik jest autoboxowany 
  na typ `Integer`
Javsko jebane gówno!

## java.util.stream.Stream
```
// grupowanie i liczenie
Stream.of(“a”, “a”, “b”, “c”, “c”)
   .collect(Collectors.groupingBy(Function.identity(), Collectors.counting());
```
## Kotlin `data class` vs Java `record`
Immutable data class + pattern matching w Java 17+:
```
sealed interface Elo permits B, A {}

record B() implements Elo {}

record A(String x, int y) implements Elo {
   A {
      if (x == null || x.isBlank()) {
         throw new IllegalArgumentException("Param 'x' must not be null or blank!");
      }
      if (y < 0) {
         throw new IllegalArgumentException("Param 'y' must be non-negative!");
      }
   }
}

static void process(Elo elo) {
   switch (elo) {
      case B b -> {
         System.out.printf("B %s\n", b);
      }
      case A a -> {
         System.out.printf("A %s\n", a);
         System.out.printf("x: %s, y: %s\n", p.x, p.y);
      }
      default -> throw new IllegalStateException("Unexpected value: " + elo);
   }
}
```

| cecha                     | data class                                                    | record                                        |
|---------------------------|---------------------------------------------------------------|-----------------------------------------------|
| sygnatura                 | `data class Point(val x: Int, val y: Int)`                    | `record Point(int x, int y)`                  |
| konstruktor               | domyślny ze wszystkimi parametrami, możliwe wartości domyślne | ze wszystkimi parametrami                     |
| gettery i settery         | tak                                                           | tak                                           |
| immutable pola            | opcjonalnie, pola mogą być `var` lub `val`                    | tak, wszystkie pola są `final`                |
| widoczność pól            | tak, pola mogą być `public`, `private`, etc.                  | nie, wszystkie pola są `public`               |
| `toString()`              | tak                                                           | tak                                           |
| `equals()` + `hashCode()` | tak                                                           | tak                                           |
| czy może być rozszerzana  | nie                                                           | nie                                           |
| czy może dziedziczyć po   | nie                                                           | nie                                           |
| kopiowanie                | `copy()`                                                      | brak, nie ma tez domyslnych argumentow w Java |
| impl interfejs            | tak                                                           | tak                                           |

## Garbage collector
sterta = heap  
* obiekty i dane globalne
* zarządza nim GC, usuwa gdy nie ma referencji do danych

stos = stack
* zmienne lokalne i wywołania metod
* zarządzanie LIFO, last-in first-out, gdy metody zwracają wynik dane z powiązanego stosu (wywołań)
  są usuwane

### Problemy GC:
* stop-the-world - inaczej pauzy aplikacji
* fragmentacja pamięci - prowadzi w dłuższej perspektywie do nieefektywnych odczytów
* szybkość alokowania nowych obiektów

### Rodzaje GC
* Serial
  * Jednowątkowy
  * Zatrzymuje wszystkie wątki apki, żeby wyczyścić. Niezbyt dobry IRL.
  * Pierwsza impl GC w java
* Parallel
  * Wielowątkowy
  * Regularne krótkie pauzy i wysoka przepustowość (ile czasu łącznie tracimy na gc)
  * Dobry do apek batchowych
  * Domyślny do Java 8
* G1 (Garbage First)
  * Wielowątkowy 
  * Dzieli stertę na równe regiony
  * W pierwszej kolejności czyści regiony z największą liczbą danych do usunięcia
  * Balans pomiędzy pauzami i przepustowością (ile czasu łącznie tracimy na gc)
  * Jeśli heap < 32 GB
  * Dostępny od Java 8, domyślny od Java 9+
* Z
  * Krótkie pauzy (max 10ms)
  * Wielowątkowy
  * Dzieli stertę na nierówne regiony
  * Jeśli duży heap > 32 GB
  * Używa konceptu "colored pointers", równolegle z wątkami aplikacji
  * Dostępny od Java 11
* Shenandoah
  * Krótkie pauzy
  * Duże zużycie CPU
  * Jeśli duży heap > 32 GB
  * Dostępny w OpenJDK 11+

### G1 GC dzieli heap na:
* Young Gen
   * nowe obiekty
   * podzielona na: 1x Eden + 2x Survivor Space (S0, S1)
   * usuwane w Minor GC (lub przenoszenie do Old Gen)
* Old Gen
   * dużej żyjące obiekty
   * usuwane w Full GC (aka Major GC)
* Metaspace (Java 8+)
   * dane o klasach i statycznych rzeczach (zamiast PremGen)

### Przydatne argumenty JVM:
 * `-Xms500m` - min pamięć sterty (heap) na 500 Mb
 * `-Xmx1g` - max pamięć sterty (heap) na 1Gb
 * `-XX:+UseParallelGC` - ustaw Parallel GC
 * `-XX:+UseG1GC` - ustaw G1 GC
 * `-XX:MaxGCPauseMillis=200` - ustawienie max czasu pauz dla G1 na 200ms

### Jak wykryć problemy z GC?
* Ustawić logi GC, np. `-Xlog:gc*:file=gc.log:time,uptime,level,tags`
* Monitorować w grafanie i prometeuszu
* Heap Dump
* Profilowanie

Szukamy:
* Częstych Full GC
* Długo trwających pauz
* Wysokiej alokacji pamięci połączonej z brakiem odzyskiwania pamięci 
* W Heap dumpie, czy nie ma dużej fragmentacji pamięci

Fragmentacja pamięci psuje wydajność bo:
* problemy z zapisem dużych obiektów - może nawet wymagać Full GC
* w skrajności może powodować OOM nawet jeśli pamięć teoretycznie jest jeszcze dostępna
* częstsze gc i potrzeby defragmentowania regionów, zabiera CPU i spowalnia apkę

# Spring
## Nowości
| wersja Spring Boot | co dodano?                                            |
|--------------------|-------------------------------------------------------|
| 3                  | Java 17+, Spring Framework 6, OpenTelemetry, Graal VM |
|                    |                                                       |

## `@Transactional`
* Metoda musi być publiczna i musisz ją wywołać przez spring proxy bean (prywatne metody
  oraz bezpośrednie wywołanie nie działają, bo wtedy nie dziala proxy)
* Rollback jest tylko dla `RuntimeException`, albo wyjątków ustawionych w `rollbackFor`

Konfiguracja:
```java
@Component
public class MyBean { // public and open to be a spring proxy
   
   @Transactional(
      isolation = READ_COMMITTED,
      propagation = REQUIRED,
      rollbackFor = {CustomException.class}
   )
   public void saveEntity() throws CustomException { // public because this method must be called by client via proxy
      // ...
   }
}
```
| propagation     | transakcja istnieje               | transakcja nie istnieje |
|-----------------|-----------------------------------|-------------------------|
| `REQUIRED`      | używa                             | tworzy nową             |
| `REQUIRES_NEW`  | zawiesza i tworzy nową            | tworzy nową             |
| `SUPPORTS`      | używa                             | bez transakcji          |
| `NOT_SUPPORTED` | zawiesza i bez transakcji         | bez transakcji          |
| `MANDATORY`     | używa                             | rzuca wyjątek           |
| `NEVER`         | rzuca wyjątek                     | bez transakcji          |
| `NESTED`        | tworzy zagnieżdżoną w istniejącej | tworzy nową             |

Zagnieżdżona transakcja:
* jej rollback nie cofa transakcji nadrzędnej
* rollback transakcji nadrzędnej, cofa też transakcje zagnieżdżoną

W JDBC `Connection` transakcja zaczyna się autmatycznie przy pierwszej wysłanej do bazy operacji,
natomiast kończy się zaraz po niej (jeśli jest ustawiony auto-commit) albo w przypadku wywołania
method `Connection.commit()` / `rollback()` / `close()`.

# K8S
## Docker compose
Buduje od nowa obrazy i restartuje wszystkie kontenery z docker compose:  
`docker-compose down && docker-compose up --build`

Czyści wolumeny i obrazy, buduje od nowa i restartuje wszystko powiązane z danym docker compose:  
`docker-compose down --volumes --rmi all`

## Pod vs Node
Node = fizyczna lub wirtualna maszyna w klastrze
posiada:
1..N podów
kublet
kube-proxy
silnik konteneryzacji (np. Docker)

Pod = abstrakcja nad kontenerami uruchomionymi na nodzie
cechy:
zawiera 1..N kontenerów z aplikacjami, ale najczęściej jeden
ma wolumen danych
ma unikalne IP w klastrze
nietrwały, może zostać wyłączony w każdej chwili

Service = abstrakcja routingu sieciowego
reprezentuje jeden punkt wejścia do grupy podów z taką samą “aplikacją”
load balancer do podów
przykrywa niedostępność podów oraz ich zmienne IP w klastrze

Kublet = monitoruje stan podów w nodzie

kube-proxy = implementacja routingu + load balancingu na każdym węźle, korzysta z konfiguracji zdefiniowanej w Service

Security
Cel
nadawca
odbiorca
szyfrowanie
tylko adresat odczyta wiadomość
klucz publiczny odbiorcy
(adresowanie)
klucz prywatny odbiorcy
(odbieranie)
podpis cyfrowy
wiem KTO wysłał wiadomość
wiem że nie została zmieniona po drodze
klucz prywatny nadawcy
(podpisywanie)
klucz publiczny nadawcy (weryfikacja)
MAC = Message Authentication Code
wiem że KTOŚ ZAUFANY wysłał wiadomość
wiem że nie została zmieniona po drodze
MAC jest szybszy niż podpis cyfrowy
współdzielony klucz (podpisywanie)
współdzielony klucz
(weryfikacja)


Internet mówi że najpierw podpisujemy, potem szyfrujemy, ALE w Modivo Ads zrobiliśmy inaczej

algorytm
typ algorytmu
rozwinięcie skrótu
użycie
AES
szyfrowanie symetryczne
Advanced Encryption Standard
HTTPS, VPN
RSA
szyfrowanie asymetryczne
+ podpis cyfrowy
  Rivest–Shamir–Adleman
  certyfikaty SSL/TLS
  HMAC
  MAC
  Hash-based Message Authentication Code
  HTTPS, generowanie kodów jednorazowych z czasu
  CMAC
  Cipher-based Message Authentic


SHA-256
Hash
Secure Hash Algorithm 256-bit


SHA-3
Secure Hash Algorithm 3
nowsze niż SHA-256 bo zminne długości
MD5
Message Digest Algorithm 5
sumy kontrolne
base62
kodowanie


zawiera dokładnie 62 znaki
brak znaków specjalnych
skracanie dużych liczb
kodowanie parametrów w URLach
generowanie krótkich unikalnych ID-ków


// sender
var message = “{top_secret: 123}”
var messageHash = hash_SHA256(message) = “djdaii”
var signature = encrypt(message_hash, senderPrivateKey) = “3n831n”
var signedMessage = “{message: ‘{top_secret: 123}’, signature: ‘3n831n’}”
var encryptedSignedMessage = encrypt(signedMessage, receiverPublicKey) = “21ds”
var sent = “{encrypted: ‘21ds’}”

// receiver
var received = “{encrypted: ‘21ds’}”
var encryptedSignedMessage = “21ds”
var signedMessage = decrypt(encryptedSignedMessage, receiverPrivateKey) =
= “{message: ‘{top_secret: 123}’, signature: ‘3n831n’}”
var message = “{top_secret: 123}”
var signature = “3n831n”
var messageHash = hashSHA256(message) = “djdaii”
var signedMessageHash = decrypt(signature, senderPublicKey) = “djdaii”
var isAuthentic = messageHash == signedMessageHash = true
Base62 vs Base64
62 zawiera tylko znaki 0-1, a-z i A-Z
Nie kompresuje danych, ALE jest bardziej zwięzły niż system dziesiętny!
Zastosowanie:
Skracanie linków
Skracanie dużych cyfr w zapisie dziesiętnych
Generowanie ładnych ID-ków do bazy danych

64 dodaje również + i / oraz = jako padding
Nie kompresuje danych (4 znaki na każde zakodowane 3 bajty)
Zastosowanie:
Przesyłanie danych w formacie tekstowym, np. załączniki do maili, parametry w JSONach
Zamiana obrazów na tekst = kodowanie danych binarnych na tekst
Kodowanie wartości nagłówków HTTP, np. Authorization: Basic <base64 credentials>

BTW system hex zawsze będzie krótszy niż dziesiętny, bo ma więcej znaków (16) więc jedna cyfra pokrywa większy zakres

Przykład:
System dziesiętny:	1234567890 		(10 znaków)
System hex: 		499602d2 		(8 znaków)
Base64:		MTIzNDU2Nzg5 	(12 znaków)
Base62:		1LY7W6 		(6 znaków)
One time password (OTP)
Np. Google Authenticator, jak to działa?
Podczas rejestracji, system generuje dla twojego konta klucz prywatny i udostępnia ci go na koniec (np. na stronie) jako kod QR
Skanujesz kod QR w aplikacji mobilnej Google Authenticator, przez co masz zapisany lokalnie klucz prywatny
Gdy chcesz się zalogować (albo system chce od ciebie potwierdzenia logowania jako 2FA), to musisz podać kod z aplikacji
Aplikacja generuje kody na podstawie obecnego czasu i pobranego wcześniej klucza prywatnego, kopiujesz ten kod i wysyłasz w formularzu do serwera. Używany algorytm to funkcja typu MAC (message auth code), np. HMAC-SHA1
Serwer ma te same dane (tzn. czas wspólny dla wszystkich oraz klucz przypisany do twojego konta), więc jest w stanie porównać kody 1:1

Tutaj przykładowy kod jak można generować takie kody: https://gitlab.com/mszczepk/sandbox/-/blob/master/modules/general/src/test/kotlin/mszczepk/sandbox/otp/OneTimePasswordTest.kt?ref_type=heads
Store passwords
https://www.youtube.com/watch?v=zt8Cocdy15c

Hasła muszą być trzymane w bazie w postaci haszu, żeby np. pracownik albo haker mając dostęp do bazy nie mógł wykraść hasła i podszyć się pod użytkownika.
Algorytm haszujący hasło musi być “bezpieczny” (np. bcrypt) - takie funkcje działają wolno, ale za to dają odporność na brut force ataki, np. md5 albo sha1 są szybkie, więc nie nadają się
Do hasła przed haszowaniem trzeba dodać “salt”, czyli losowy string, żeby:
zabezpieczyć przed atakiem z użyciem rainbow tables (czyli zbiorów wcześniej obliczonych haszy)
zapewnić unikalność hasza (kilku użytkowników może mieć te same hasło, np. dupa1234)
utrudnia życie hakerowi jeśli odgadnie samą funkcję haszującą
Salt przechowujemy w bazie danych obok hashu hasła. Może być plain text bo to nie jest secret.
Gdy użytkownik się loguje, odczytujemy sól z bazy, liczymy hasz tak samo jak przy zapisie i porównujemy z haszem zapisanym w bazie. Jak są równe tzn że hasło też jest ok.
Czyli cały pomysł polega na tym że hasz działa tylko w jedną stronę, bezpowrotnie tracimy info o oryginalnym haśle, ALE jesteśmy w stanie stwierdzić że dane logowania się zgadzają.

Hash
Celem jest przekształcenie dużego zakresu wartości w mniejszy zakres. Dlatego występują kolizje i najlepsze funkcje mają równomierny rozkład.
Funkcja
Długość
Równomierna (kolizje)
Szybkość
Krypto
Zastosowanie
MD5
16 B
Dużo kolizji
Szybka
Nie
Sumy kontrolne
SHA-1
20 B
Dużo
Wolna
Nie


SHA-256
do 64B
Mało
Wolna
Tak
Podpisy cyfrowe, SSL/TLS
SHA-3


Mało
Wolna
Tak


MurmurHash
do 16B
Średnio
Szybka
Nie
Cassandra, Hadoop
CityHash
do 16B
Średnio
Szybka
Nie



Funkcja hash jest dobra kryptograficznie jeśli:
Brak kolizji - mało wartości które mają ten sam hasz
Brak drugorzędnych  kolizji - nie da rady wywnioskować wartości hash znając przykładową wartość oryginalną i jej hash
Jednostronna - nie można odgadnąć wartości oryginalnej znając hasz
Niewielka zmiana danych powinna dawać drastycznie różne wartości haszu (efekt lawinowy)
Session vs JWT
Sesja to dane o użytkowniku trzymane na backendzie. Klient trzyma w ciastku tylko id sesji, dzięki czemu serwer może sobie odczytać dane z sesji, np. role, dostępy.

“sticky session” - jak masz LB to wtedy przekierowuje zawsze do tej instancji usługi która ma twoją sesję, a nie do “losowej”, jest to ważne jeśli dane sesji są trzymane in-memory na serwerze zamiast w rozproszonym keszu (Redis) albo w bazie.

JWT
Podpisane cyfrowo dane trzymane na kliencie.
Opcjonalnie mogą być zaszyfrowane żeby zwiększyć bezpieczeństwo - wtedy jest to JWE.
Mamy pewność że nie zostały zmienione (podpis cyfrowy), więc gdy klient wysyła je w nagłówku, to serwer może sobie odczytać np. role i dostępy.
Lepiej się skaluje niż sesje, bo nie wymaga rozproszonego kesza, ani synchronizacji sesji lub sticky-sessions.
Ale za to trudniej je unieważnić, bo klient trzyma u siebie.
Database
Typy baz
Transactions per second
Typ bazy / storage
Przykład
Charakterystyka
1k - 10k
Tradycyjne relacyjne SQL
Postgres, MySQL, Oracle


10k - 100k
Nie relacyjne
MongoDB, Cassandra, RabbitMQ


100k - 1M
in-memory
Redis, Elastic, Kafka




Typy baz:
klucz-wartość (np. Redis)
cache
bez skomplikowanych query
kolejki pub/sub !
leaderboards (np. proste listy per klucz, można apendować wartości)
bez schemy
kolumnowe (np. Cassandra, HBase)
dane związane z czasem (np. z IoT, sensorów, komentarze do livestream)
dane historyczne
dużo zapisów, mało odczytów
dokumentowe (np. MongoDB)
generalne przechowywanie
bez joinów, dane (dokumenty) muszą być możliwie zdenormalizowane
bez schemy
relacyjne (np. Postgres)
joiny i normalizacja
schema
grafowe (np. Neo4j)
silniki rekomendacji
grafy, social media, relacje
antyfraudy
full text search (np. Elastic Search, Solr)
podobne strukturalnie do dokumentowych, tylko że robi odwrócone indeksy (inverted index) z wartości
Sharding
Dzielenie dużej bazy danych na mniejsze “shardy”. Każdy shard odpowiada za pewien zakres danych.
Podział danych może być ze względu na:
Wartość klucza - np. każdy shard zawiera jakiś przedział kluczy
Geolokalizację albo ID użytkownika

Najprostsze shardowanie to gdy znamy liczbę shardów N (nodów/maszyn/etc)
whichNode = hashCode(key) % N
Nie jest to idealne jeśli zmienia się liczba węzłów (np. jeśli padnie node).

Powiązany temat to “sharded counter” - każdy węzeł przechowuje część licznika którą podbijasz lokalnie, jeśli chcesz odczytać wartość obecną licznika to musisz pobrać i zsumować ze wszystkich węzłów, optymalizuje to zapisy kosztem wolniejszego odczytu
Consistent sharding (hashing)
https://www.youtube.com/watch?v=UF9Iqmg94tk
Jest to typ hashowania używany w shardowaniu jeśli liczba nodów jest zmienna. Polega to na tym że nie robimy zwykłego modulo bo to wymagałoby przenoszenia dużej liczby danych między nodami.

Używamy struktury danych która przypomina okrąg i dzielimy go na wiele części. Każdy node może mieć wiele części okręgu. Jeśli jakiś węzeł pada, to wszystkie jego klucze przechodzą pod zarządzanie do węzłów, które są obok tych fragmentów. Dzięki temu nie musimy przenosić w systemie wszystkich wartości tylko mniejszy podzbiór.

Multi-region deployment database
Celem jest zwiększenie dostępności oraz przyspieszenie odczytu danych w zależności od regionu użytkownika.
Można po prostu podzielić system na regiony, które siebie nie widzą (jeśli jest to ok biznesowo).
Można też zapewnić jakiś sposób replikacji danych między regionami.

Bazy danych wspierające replikację między regionami i przełączanie na inny region w razie awarii:
Amazon Aurora (rozproszona SQL)
Google Cloud Spanner (rozproszona SQL)
CockroachDB (rozproszona SQL)
Microsoft Azure Cosmos DB (noSQL)
MongoDB Atlas (noSQL)
Couchbase (noSQL)
Cassandra (NoSQL) - ale tylko eventual consistency
Redis Enterprise
CDC (Change Data Capture)
Baza źródłowa (np. Postgres) wysyła strumień informacji o zmianach (operacje write) do innej bazy analitycznej (np. Elastic, BigQuery, Kafka).
Są różne narzędzia do CDC, np.
Debezium - open-source narzędzie do pobierania danych od źródła do Kafki, wspiera log-based CDC, czyli czytanie z logów transakcji
Kafka Connect - narzędzie żeby z Kafki przerzucać dane do docelowej bazy
AWS DMS (Database Migration Service)

Więc przykładowo można zrobić: MongoDB <- Debezium -> Kafka <-Kafka Connect -> Elastic

Rodzaje CDC:
transaction log based - patrzy na dziennik transakcji, wydajne podejście, każda baza ma swój dziennik, np. Postgres ma WAL (write ahead log), MongoDB ma Oplog (operation log)
state based - porównują stany baz, często używane w hurtowniach danych
Distributed transaction

Redis
https://architecturenotes.co/p/redis

Redis Cluster:
Sharding - dany shard zawiera tylko podzbiór danych z całego systemu.
Deterministyczne haszowanie kluczy - sprawia że zawsze wiemy, w którym shardzie spodziewać się danego klucza.
Resharding, czyli nowe rozdzielenie kluczy między shardami jeśli dodaliśmy nowy shard.
Hash sloty - żeby resharding był wydajny, czyli każdy shard jest powiązany z jakimś przedziałem hashy (a nie określonym hashem) i dzięki temu część danych może zostać na starym shardzie, a trzeba wymienić się tylko tymi które już nie pasują.
Shard - to z reguły jeden serwis (master) ale można ustawić mu replikację (slaves). Domyślnie klienci zapisują i odczytują tylko z mastera w danym shardzie (tak samo jak w Kafce), a slave służą jako failover i kopie zapasowe. Ofc można zmienić tę konfigurację żeby czytać też z replik żeby zwiększyć availability.

Redis jako baza key-value, trzeba użyć dwóch trybów żeby nie stracić danych po restarcie:
RDB (Redis Database Backup) - robione są snapshoty danych w interwałach
(+) wydajne
(-) dane mogą zostać utracone między interwałami
AOF (Append Only File) - każda operacja “write” jest zapisywana na dysku - appendowana do logu z którego Redis może sobie odtworzyć stan
(+) prawie pełna persystencja
(-) wolniejsze
(-) plik logów może rosnąć szybko, dlatego warto włączyć opcję kompresji, albo dorzucić snapshoty
Cassandra
https://www.slideshare.net/slideshow/cassandra-introduction-features-30103666/30103666
Jest peer to peer, nie ma master-slave.
Jest high availability (HA)
Nie ma one point of failure
Skaluje horyzontalnie i liniowo, tzn. więcej węzłów = większa przepustowość
Ładnie skaluje się do geograficznych regionów
Przy około 300 węzłach można oczekiwać wydajności 1M zapisów /s - bardzo dużo!!
Przy około 50 węzłach - ok 200k zapisów /s
Można wybrać między strong/eventual consistency
replication factor < (write nodes + read nodes) - silna spójność
Dane są trzymane jakby w hashmapie z kolumnami
Map<HashableRowKey, SortedSet<ColumnKey, ColumnValue>>
klucze powinny być haszowalne żeby dystrybuować dane równo w klastrze, tak się robi partycjonowanie
Zoptymalizowane do zapisów (zapisy są append only), np. IoT, statystyki, analizy danych
Kafka


Jak usunąć stare wiadomości z kafka?
Nie da się bezpośrednio usunąć, trzeba przyjąć inne podejście, np. ustawić na Kafce retencję danych oraz dodatkowy serwis (archiving service) który konsumuje dane z Kafki i zapisuje je do archiwum (innej bazy danych np. Cassandry). Inna opcja to ustawić kompaktowanie topików, czyli grupować wiadomości do jednego klucza i zostawiać najnowszy rekord.

Fault tolerance (odporność na awarie węzłów)
Klaster składa się z koordynatora (Zookeeper, Kafka Raft) i węzłów na których są zainstalowane brookery (jeden brooker per węzeł). Koordynator sprawdza, które brookery są dostępne i zarządza relacjami leader-follower.
Każdy topik jest dzielony na partycje i partycje są rozkładane między brookery. Niekoniecznie po równo, ale tak żeby zrównoważyć obciążenie.
Dla każdej partycji wybierany jest brooker który będzie jej liderem. Pozostałe brookery są followerami dla tej partycji. Każdy brooker może być liderem dla jednej lub wielu partycji - to też jest rozkładane w miarę równomiernie.
Jeśli jakich brooker z leaderem ma awarię to wybierany jest nowy leader.
Leader obsługuje zapisy i odczyty dla danego topiku. Wszyscy klienci zostają automatycznie przekierowani.
Liczbę replik partycji można ustawić replication.factor
2 - (1 leader + 1 follower) minimalna liczba replik żeby zapewnić odporność na awarię leadera
3 - (1 leader + 2 followers) typowe ustawienie
4+ - dla bardziej krytycznych systemów, ale wiadomo rosną opóźnienia i koszty przechowywania danych
W konfiguracji producenta można ustawić ile replik musi potwierdzić zapis żeby uznać go za trwały (durable)
acks=0 - producer nie czeka na potwierdzenie zapisu od brokera, utrata danych gdy padnie lider albo gdy wystąpi błąd
acks=1 - leader zapisuje wiadomość lokalnie, utrata danych gdy padnie lider
acks=all - producent wysyła wiadomość, leader zapisuje w swoim logu (opcjonalnie flushuje na dysk), czeka aż wszystkie repliki ISR pobiorą od niego te dane (nie czeka na repliki out-of-sync), jak ma już wszystkie potwierdzenia to zwraca potwierdzenie do producenta
enable.idempotence=true - to dodatkowe zabezpieczenie producera (ustawione by default), jeśli nie odbierze od lidera potwierdzenia zapisu (np. zerwie się połączenie), to żeby producent nie próbował wysłać wiadomość jeszcze raz co prowadziłoby do duplikatów

Dlaczego nie przesadzać z liczbą replik partycji?
Bo więcej replik większe opóźnienia i koszty bo:
Dane trzeba skopiować na wszystkie dyski w klastrze, w przypadku Tb danych robi to różnice pomnożyć wszystko xN
każdy follower musi pobrać dane z leadera, co wysyca przepustowość oraz zawsze trochę obciąża leadera
wiadomość jest dostępna dla konsumera dopiero jak wszystkie repliki potwierdzą zapis, jeśli followersi są in-sync (in-sync replica - ISR) to nie ma tragedi, lag = lagowi synchronizacji, ALE jeśli np. replika padła i musi od zera dociągnąć wszystko z leadera (bo jest totalnie out-of-sync) to trzeba poczekać aż się zsynchronizuje, to może potrwać

Co jeśli mam 3 repliki (1 leader + 2 followers) i padną wszyscy followersi?
Leader będzie działać po nie potrzebuje replik, sam obsługuje read i write
Jeśli repliki wrócą do życia to zostaną zsynchronizowane z leaderem
Jeśli leader też padnie to zależy od konfiguracji, bo domyślnie buforuje przyjęte wiadomości i zapisuje je na dysku co jakiś czas. Można skonfigurować parametry log.flush.interval.(ms|messages) ale w praktyce są one dobrane tak że zapisy są dość rzadko żeby zwiększyć wydajność i polegać na synchronizacji z followersami.
Locking
Distributed Lock
np. Redis - po prostu zapisujesz rekord w rozproszonym cache albo bazie i przed wykonaniem operacji sprawdzasz czy takiego rekordu nie ma (tak, Redis umożliwia atomowy odczyt i zmianę wartości klucza). Inny przykład to tabele lock w liquibase. Trzeba tylko obsłużyć TTL żeby lock nie został na zawsze.

Notifications
Push
Firebase Cloud Messaging (FCM) -> Android
Apple Push Notification Service (APNS) -> iOS
WebPush - na przeglądarkę

Aplikacja na telefonie rejestruje się w systemie notyfikacji i otrzymuje swoje ID do notyfikacji.
Push nie ma 100% gwarancji na dostarczenie z różnych powodów:
Apka mobilna ma tak skonfigurowane ustawienia, np. battery saver mode, do not disturb
Duże obciążenie na serwerach przez co duże opóźnienia
Sam serwer push zwraca naszej usłudze BE status czy notyfikacja została przyjęta i że będą próby dostarczenia.

Email
SMTP Server (Simple Mail Transfer Protocol) - serwer dostarcza email
np. SendGrid, Mailgun, Amazon SES

SMS
SMS Gateway (np. Twillo, Nexmo) - przekazuje wiadomość do operatora komórkowego
WebSocket vs Server Sent Events (SSE)
Persystentne połączenie z serwerem. Musi wspierać to infra oraz czasem trzeba keszować i wiedzieć jakiego klienta obsługuje która instancja usługi (gdy skalujemy horyzontalnie).

WebSocket
dwukierunkowy
ma swój protokół, nie HTTP
mogą być dane binarne i tekstowe
infrastruktura (np. LB, API Gateway, Firewall, Proxy) muszą wspierać przejście połączenia HTTP w WebSocket
trzeba ręcznie zaimplementować wznawianie połączeń
dobre do chatów i gier gdzie klient chce wysyłąć dużo danych do serwera
natywne aplikacje mobilne wspierają w libkach

SSE
jednostronne (tylko serwer -> klient)
oparte na HTTP
tylko tekstowy format danych (UTF-8)
po prostu długo trwające żądanie HTTP (serwer informuje nagłówkami że będzie wysyłać odpowiedź w chunkach)
dobre do notyfikacji użytkownika
przeglądarki automatycznie wznawiają połączenie
natywne aplikacje mobilne nie wspierają, trzeba korzystać z bibliotek albo samemu zaimplementować
odpowiednia konfiguracja infry, proxy i load balancerów: Sticky Sessions, connection: keep-alive, wydłużyć timeouty, wyłączyć buforowanie

WebSocket trzeba przełączyć z obecnego połączenia HTTP do serwera:
klient wysyła HTTP Upgrade
GET /index.html HTTP/1.1
Host: www.example.com
Connection: upgrade
Upgrade: websocket
serwer odpowiada ze specjalnym nagłówkiem 101, oraz w zmienionym protokole (np. w body?)
HTTP/1.1 101 Switching Protocols
Upgrade: foo/2
Connection: Upgrade
z punktu widzenia JS, jest to dość proste, cały HTTP handshake i przełączenie dzieje się automatycznie:
const socket = new WebSocket("wss://example.com/socket");

SSE:
klient łączy się HTTP na endpoint z odpowiednim nagłówkiem
serwer wysyła w odpowiedzi nagłówek Content-Type: text/event-stream
Połączenie zostaje otwarte i serwer może wysyłać dane w formacie tekstowym data: <wiadomosc>\n\n



